// my HTML selectors

const elemSubmitButton = document.querySelector(".submitButton"); // const, elem
const elemAlphaButton = document.querySelector(".alphaButton");
const elemTextForm = document.querySelector(".formText");

//html selectors of innerText value in each form
const elemNameInput = document.querySelector(".nameInput");
const elemCostInput = document.querySelector(".costInput");
const elemQuantityInput = document.querySelector(".quantityInput");
// empty div for our list items to be added to
const elemPushedList = document.querySelector(".PushedList");
//mostly empty table for data to be entered
const elemMyTable = document.querySelector(".myTable");

// My array that has my object of listItems pushed into it
let myItemsArr = [];
let filteredArray = [];
//let newArray = myItemsArr.reverse();
function addToDom(inputItems) {
  let unsortedList = document.querySelectorAll(".appendedLis");
  for (i = 0; i < unsortedList.length; i++) {
    unsortedList[i].remove();
  }
  for (i = 0; i < inputItems.length; i++) {
    //if empty string (if text = 0?) do not append
    let tableRow = document.createElement("tr");
    let nameItem = document.createElement("td");
    let costItem = document.createElement("td");
    let quantityItem = document.createElement("td");
    let totalItem = document.createElement("td");
    nameItem.classList.add("appendedLis");
    costItem.classList.add("appendedLis");
    quantityItem.classList.add("appendedLis");
    tableRow.classList.add("appendedTr");
    totalItem.classList.add("appendedLis");
    nameItem.innerText = inputItems[i].elemNameInput;
    costItem.innerText = inputItems[i].elemCostInput;
    quantityItem.innerText = inputItems[i].elemQuantityInput;
    totalItem.innerText = inputItems[i].cashedTotal;
    tableRow.appendChild(nameItem);
    tableRow.appendChild(costItem);
    tableRow.appendChild(quantityItem);
    tableRow.appendChild(totalItem);
    elemMyTable.appendChild(tableRow);
    //loop through my array and append each item to the dom with each interval
  }
} // verbs

// function filterfunc(unfilteredArray) {
//   for (i = 0; i < unfilteredArray.length; i++) {
//     for (j = 0; j < unfilteredArray.length; j++) {
//         let match = false;
//       if (unfilteredArray[i] === unfilteredArray[j]) {
//         match = true;
//       });
//     } filteredArray.push(unfilteredArray[i]
//   }
//   return filteredArray;
// }
//pushes object into my "myItemsArr" array when submit button clicked

elemSubmitButton.addEventListener("click", function (event) {
  event.preventDefault();
  const inputObj = {
    elemNameInput: elemNameInput.value,
    elemCostInput: elemCostInput.value,
    elemQuantityInput: elemQuantityInput.value,
    cashedTotal: (
      parseFloat(elemCostInput.value) * parseFloat(elemQuantityInput.value)
    ).toFixed(2),
  };
  myItemsArr.push(inputObj);

  // where the duplicates need to be filtered
  //filterfunc(myItemsArr);
  console.log("input obj", inputObj);
  // let noDups = ;
  addToDom(findAndReplace(myItemsArr, inputObj));
  // clears enter text out of input field and resets them to nothing
  elemNameInput.value = "";
  elemCostInput.value = "";
  elemQuantityInput.value = "";
});

function findAndReplace(arr, incomingObj) {
  let copyArr = arr;
  for (let i = 0; i < copyArr.length; i++) {
    let personObj = copyArr[i];
    const personName = personObj.elemNameInput;

    if (incomingObj.elemNameInput === personName) {
      console.log("match");
      copyArr[i] = incomingObj;
      // myItemsArr.push(inputObj);
    }
  }
  return copyArr;
}

elemAlphaButton.addEventListener("click", function (e) {
  e.preventDefault();
  // sorts my cost items field
  sortedArray = filteredArray;
  sortedArray.sort(function sortMyItems(a, b) {
    if (parseFloat(a.cashedTotal) > parseFloat(b.cashedTotal)) {
      return 1;
    } else if (parseFloat(a.cashedTotal) < parseFloat(b.cashedTotal)) {
      return -1;
    }
    return 0;
  });
  //removes my appended list items
  let unsortedList = document.querySelectorAll(".appendedLis");
  for (i = 0; i < unsortedList.length; i++) {
    unsortedList[i].remove();
  }
  addToDom(sortedArray);
  //   for (i = 0; i < sortedArray.length; i++) {
  //     let tableTotal = document.createElement("td");
  //     tableTotal.innerText = sortedArray[i];
  //     elemMyTable.appendChild(tableTotal);
  //   }
});

// create an empty array
// dynamically create an object containing our desired fields and push it into our array

// declare extra variable holding an empty array to hold the total cost value
// let totalListCost =[]; or set equal to 0?

// eventListener on Submit Button will:
//push text field.value into empty array it is associated with....  myItemsArr.push(elemTextForm.value);
//call function
// clear all txt fields

// function will:
//create 3 loops through each looping through the array
// the loop will create a new li
//li will be pushed into the ul
//class name for ul will need to be created for stlying
// the total cost will be set to the cost.value * quantity.value....to number?

//second function:
